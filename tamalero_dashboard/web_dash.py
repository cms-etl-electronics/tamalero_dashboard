import requests
import json
import plotly.express as px
import dash_bootstrap_components as dbc
from dash import Dash, Input, Output, callback, dcc, html, ctx, MATCH, ALL, Patch
from dash_bootstrap_templates import load_figure_template

from datetime import datetime
import zipfile
import json

def make_request(value: str, payload={}, port=5000, url="http://127.0.0.1") -> dict:
    if payload:
        request = requests.post(f"{url}:{port}/{value}", data=json.dumps(payload))
    else:
        request = requests.get(f"{url}:{port}/{value}")
    return request.json()


# API Request Processing Functions; Make API Request; Return format which can be plotted/updated


def etroc_status(name: str) -> list:
    # Return list of matrix for each ETROC on connected module
    etroc_status_dict = make_request(name)
    etroc_status_list = []
    for module in etroc_status_dict.values():
        for etroc in module.values():
            etroc_status_list.append(etroc)
    return etroc_status_list

# Layout Functions; Generated Dash based on module_links API return


def create_dropdown(name: str, elements: int, label: str, module: int):
    dropdown = []
    for index in range(elements):
        ID = {"name": "dropdown", "module": module, "etroc": index}
        dropdown.append(dbc.DropdownMenuItem(f"{name} {index + 1}", id=ID))

    return dbc.DropdownMenu(children=dropdown, label=label, className='my-1')


def create_etroc_layout(module_status: dict) -> list:
    # Creates the Layout and Callback for ETROC Graphs
    heading_style = "text-center"
    green_text = "text-success text-center"
    red_text = "text-danger text-center"
    elink_style = "d-inline-block w-50"
    graph_style = "ratio ratio-1x1 w-100"
    column_style = "p-0 border rounded border-primary border-5 "

    etroc_layout = [
        html.H1('ETROC Status', className=heading_style),
        dbc.Button('Check ETROC Status', id='etroc_button', className='btn btn-lg my-1'),
    ]

    for module, status in module_status.items():
        num_etroc = len(status.keys()) - 1
        # num_etroc = 4
        if status['connected']:
            etroc_layout += [
                html.H2(f"Module {int(module) + 1}", className=green_text),
                create_dropdown("ETROC", num_etroc, "Run Threshold Scan", module)
            ]
        else:
            etroc_layout += [html.H2(f"Module {int(module) + 1}", className=red_text)]

        row = []
        for etroc in range(num_etroc):
            elink_heading = f"text-center text-decoration-underline {elink_style}"
            col = [
                html.H3(f"ETROC {etroc + 1}", className="text-center"),
                html.H4("elink 1", className=elink_heading),
                html.H4("elink 2", className=elink_heading),
            ]
            for elink, stat in status[f"{etroc}"].items():
                elink_color = green_text if stat['locked'] else red_text

                elink_className = f"{elink_color} {elink_style}"
                col.append(
                    html.H4(f"lpGBT {stat['lpGBT'] + 1}: {stat['elink']}", className=elink_className),
                )
            if status['connected']:
                etroc_graph_id = {"name": "etroc-graph", "module": module, "etroc": etroc}
                threshold_div_id = {"name": "threshold-div", "module": module, "etroc": etroc}
                threshold_data_id = {"name": "threshold-data", "module": module, "etroc": etroc}
                threshold_heatmap_id = {"name": "threshold-heatmap", "module": module, "etroc": etroc}
                threshold_graph_id = {"name": "threshold-graph", "module": module, "etroc": etroc}
                graph_modal_id = {"name": "graph-modal", "module": module, "etroc": etroc}

                threshold_modal = dbc.Modal([
                    dbc.ModalHeader(
                        dbc.ModalTitle(f"Threshold Scan: Module {module} | ETROC {etroc + 1}"),
                        close_button=True),
                    dbc.ModalBody(dcc.Graph(id=threshold_graph_id))],
                    id=graph_modal_id, centered=True, is_open=False, size='xl')

                col += [
                    dbc.Spinner(
                        dcc.Graph(id=etroc_graph_id, className=graph_style,
                                  loading_state={"prop_name": 'figure', 'is_loading': True}),
                        spinner_style={"width": "4rem", "height": "4rem"},
                    ),
                    dcc.Store(id=threshold_data_id, storage_type='memory'),
                    html.Div([
                        html.Hr(className="border rounded border-1 border-primary my-1"),
                        html.H4("Peak Values of Threshold Scan", className="text-center"),
                        threshold_modal,
                        dbc.Spinner(
                            dcc.Graph(id=threshold_heatmap_id, className=f'{graph_style}',
                                      config={'displayModeBar': False}),
                            spinner_style={"width": "4rem", "height": "4rem"}
                        )
                    ], id=threshold_div_id, className="d-none")
                ]

            row.append(dbc.Col(col, md=6, lg=3, className=column_style))

        etroc_layout.append(dbc.Row(row, justify='center', className="mb-5"))

    return html.Div(etroc_layout, id='layout')


def update_etroc_fig(etrocs: int) -> list:
    # IDEA: Pass the dictionary instead, where you check if the id matches the JSON return. I.e the
    # request for an ETROC on a particular module matches the return of that ETROC on that module.
    # for module, etroc = module,etroc .. append to list.
    etroc_status_list = etroc_status('etroc_status')
    figures = []
    for etroc in range(etrocs):
        figures.append(px.imshow(etroc_status_list[etroc], title="", text_auto=True,
                                 color_continuous_scale=['#e74c3c', '#00bc8c'], range_color=[0, 1],
                                 aspect='equal', origin='lower'))

        figures[etroc].update_layout(coloraxis_showscale=False, font={"size": 20},
                                     margin={"l": 5, "r": 10, "t": 0, "b": 5, "pad": 2})
    return figures


# The entire layout is created on a single request from module_links endpoint.
# The graphs are updated via there API endpoints

module_link_status = make_request('module_links')

etroc_layout = create_etroc_layout(module_link_status)

# Theme
load_figure_template("flatly")

# Initialize the app
app = Dash(__name__, external_stylesheets=[dbc.themes.FLATLY])
app.title = "Readout Board Dash"

heading_style = "text-center"

layout = [
    dbc.Button('Export Summary', id='summary_button', n_clicks=0),
    dcc.Download(id="download-summary"),
    html.H1(children='Temperatures', className=heading_style),
    dbc.Button('Start Monitoring', id='temperature_button', n_clicks=0, className='btn btn-lg my-1'),
    dcc.Store(id='temperature-data', storage_type='session'),
    dcc.Graph(id='temperature-graph-content', className='border rounded border-primary border-5'),
    dcc.Interval(id='interval-component', interval=1 * 1000),  # interval in milliseconds
    html.Br(),
    etroc_layout
]

app.layout = dbc.Container(layout, fluid=True, className="p-5")  # Fluid Sets to Full Width


@callback(
    Output("download-summary", "data"),
    Input("summary_button", "n_clicks"),
    prevent_initial_call=True,
)
def download_summary(n_clicks):
    #define validator verison here
    val_version = '1.0'

    summary_data = make_request('summary_tests')
    print(summary_data)
    utcnow = datetime.utcnow()
    #need to make zip file that downloads a file for each module
    def write_summary(bytes_io):
        with zipfile.ZipFile(bytes_io, 'w') as zipF:
            # Iterate over each mod in the dict
            for mod in summary_data:
                summary_data[mod]['version'] = val_version
                str_data = json.dumps(summary_data[mod])
                bytes_data = str_data.encode('utf-8')
                filename = f"module_{mod}.json"
                # Add the JSON file data to the Zip file
                zipF.writestr(filename, bytes_data)

    # try:
    #     #currently does not work because the starting key is variable
    #     test_object = version_lookup[val_version](**summary_data) #gets pydantic data validator object
    #     print('SUCCESSFUL VALIDATION')
    # except ValidationError as e:
    #     #Need to make this a flash error so that people know if there was a validation issue
    #     print('----------------------------------')
    #     print(e.errors())

    #dcc.send_bytes to send zip file to dcc.Downloads!
    return dcc.send_bytes(write_summary, f"ELT_Test_Summary_{utcnow}.zip") #clear after call, needs to prevent mixing old and new tests between modules

@callback(
    Output('temperature-data', 'data'),
    Output('temperature-graph-content', 'figure'),
    Output('temperature_button', 'children'),
    Output('temperature_button', 'className'),
    Input('temperature_button', 'children'),
    Input('temperature_button', 'n_clicks'),
    Input('interval-component', 'n_intervals'),
    Input('temperature-data', 'data'),
)
def temperature_monitoring(button_text, n_clicks, n_intervals, temperature):
    # This is run every second, based on n_intervals
    # TODO see if using a patch here makes more sense also
    if n_clicks % 2 == 0:
        temperature = {}
        button_text = 'Start Monitoring'
        button_style = 'btn btn-lg my-1 btn-primary'
        return temperature, {}, button_text, button_style
    else:
        if not temperature:
            current_temperature = make_request('rb_temp')
            for value in current_temperature:
                temperature[value] = [current_temperature[value]]
        else:
            for value in temperature:
                temperature[value].append(make_request('rb_temp')[value])

        button_text = 'Stop Monitoring'
        button_style = 'btn btn-lg my-1 btn-danger'

        temperature_fig = px.line(temperature, x='time', y=['t1', 't2', 't_VTRX', 't_SCA'],
                                  markers=True, title='Sensor Temperatures',
                                  labels={'time': 'Time(HH:MM:SS)', 'value': 'Temperature(C)'})

        temperature_fig.update_traces(line={'width': 4}, hovertemplate="%{y:.1f}")
        temperature_fig.update_layout(font={'size': 20}, hovermode="x unified")
        temperature_fig.update_xaxes(tickformat="%-I:%M:%S %p", nticks=8)

        return temperature, temperature_fig, button_text, button_style


@callback(
    Output({'name': 'etroc-graph', 'module': ALL, 'etroc': ALL}, 'figure'),
    Output('etroc_button', 'children'),
    Input('etroc_button', 'n_clicks'),
    prevent_initial_call=True
)
def check_etroc_status(n_clicks):
    # print(ctx.outputs_list)
    print(ctx.outputs_list[0][0])

    total_num_etroc = len(ctx.outputs_list[0])
    etroc_figures = update_etroc_fig(total_num_etroc)
    button_text = 'Recheck ETROC Status'
    return etroc_figures, button_text


@app.callback(
    Output({'name': 'threshold-div', 'module': MATCH, 'etroc': MATCH}, 'className'),
    Input({'name': 'dropdown', 'module': MATCH, 'etroc': MATCH}, 'n_clicks'),
    prevent_initial_call=True
)
def show_threshold_row(n_clicks):
    return ""


@app.callback(
    Output({'name': 'graph-modal', 'module': MATCH, 'etroc': MATCH}, 'is_open'),
    Output({'name': 'threshold-graph', 'module': MATCH, 'etroc': MATCH}, 'figure'),
    Output({'name': 'threshold-data', 'module': MATCH, 'etroc': MATCH}, 'data'),
    Output({'name': 'threshold-heatmap', 'module': MATCH, 'etroc': MATCH}, 'figure'),
    Input({'name': 'dropdown', 'module': MATCH, 'etroc': MATCH}, 'n_clicks'),
    Input({'name': 'threshold-heatmap', 'module': MATCH, 'etroc': MATCH}, 'clickData'),
    Input({'name': 'threshold-data', 'module': MATCH, 'etroc': MATCH}, 'data'),
    prevent_initial_call=True
)
def toggle_modal(threshold_dropdown, click_data, threshold_data):
    if not threshold_data or ctx.triggered_id['name'] == 'dropdown':
        # TODO You need to make the API request for that specific MODULE/ETROC here
        etroc = ctx.triggered_id['etroc']
        module = ctx.triggered_id['module']

        payload = {'module': module, 'etroc': etroc}
        print(f"ETROC {etroc} on Module {module} \n")

        threshold_data = make_request('threshold_scan', payload=payload)


        max_matrix = threshold_data['max_matrix']
        threshold_heatmap = px.imshow(max_matrix, text_auto=True, aspect='equal',
                                      color_continuous_scale=['#e74c3c', '#00BC8c'], origin='lower')

        threshold_heatmap.update_traces(textfont_size=12,
                                        hovertemplate="Pixel:  %{x}, %{y} <extra></extra>")
        threshold_heatmap.update_layout(coloraxis_showscale=False, font={"size": 17},
                                        margin={"l": 2, "r": 10, "t": 0, "b": 0, "pad": 0})

        return False, {}, threshold_data, threshold_heatmap

    if ctx.args_grouping[1]['triggered']:
        x = click_data['points'][0]['x']
        y = click_data['points'][0]['y']
        peak_value = click_data['points'][0]['z']

        pixel_value = y + (x * 16)

        vth_axis = threshold_data['vth_axis']
        hit_rate = threshold_data['hit_rate'][pixel_value]

        highlighted_cell = {'editable': True, 'line': {'color': 'yellow', 'width': 2.3},
                            'opacity': 1, 'x0': x - .46, 'x1': x + .46, 'xref': 'x',
                            'y0': y - .46, 'y1': y + .46, 'yref': 'y'}

        cell_annotation = {'font': {'color': 'yellow', 'size': 12}, 'showarrow': False,
                           'text': f'<b>{peak_value}<b>', 'x': x, 'xref': 'x', 'y': y, 'yref': 'y'}

        threshold_heatmap = Patch()
        threshold_heatmap['layout']['shapes'][0] = highlighted_cell
        threshold_heatmap['layout']['annotations'][0] = cell_annotation

        fig_title = f'Pixel {x}, {y}, &nbsp; Peak Value: {peak_value} '

        threshold_fig = px.line(x=vth_axis, y=hit_rate, title=fig_title, markers=True,
                                labels={'x': 'Vth', 'y': 'hit rate'})

        # These may be useful once the thresholds become S-curves
        # threshold_fig.add_vline(x=840, line_width=3, line_dash="dash",
        #                         line_color="red", annotation_text=" Baseline",
        #                         annotation_font_color="red")
        # threshold_fig.add_vrect(x0="830", x1="850", fillcolor="red", opacity=0.25, line_width=0)

        threshold_fig.update_layout(font={'size': 20})
        return True, threshold_fig, threshold_data, threshold_heatmap


# Run the App
if __name__ == '__main__':
    app.run_server(debug=True)
