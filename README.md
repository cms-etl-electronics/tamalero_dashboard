# Tamalero Dashboard

![WebDash Board](dashboard.png)

## Dependencies
Tested on python 3.8.10.

```shell
git clone https://gitlab.cern.ch/cms-etl-electronics/tamalero_dashboard.git
pip install -r requirements.txt
```


## Running Dashboard
[Tamalero](https://gitlab.cern.ch/cms-etl-electronics/module_test_sw) needs to run with arguments
`--server --port 5000` for dashboard to communicate with API.

```shell
cd tamalero_dashboard
python web_dash.py
```
